#!/bin/bash

#PAGEURL="http://www.spautores.pt/comunicacao/noticias/mais-de-uma-centena-de-autores-e-artistas-exigem-nova-lei-da-copia-privada"
PAGEURL="http://www.spautores.pt/comunicacao/noticias/mais-de-duas-centenas-de-autores-e-artistas-exigem-nova-lei-da-copia-privada"
REPO=~/spa_ministryoftruth
OUTPUTFILE=$REPO/lista_raw.html
NOW=$(date '+%Y%m%d%H%M')

cd $REPO

/usr/bin/curl -f "$PAGEURL" > $OUTPUTFILE
cat $OUTPUTFILE | awk '/<div id="content-mid"/,/<div class="clear">/' > $REPO/lista.html

if [ "0" -ne "$?" ] ; then
	echo "Failed to load page from $PAGEURL"
	exit 1
fi

/usr/bin/git commit -a -m "v$NOW"
/usr/bin/git push origin master
